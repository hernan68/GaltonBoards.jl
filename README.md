# GaltonBoards

This package provides base types and functionality to build simulations for Galton boards.

## Introduction

A Galton board according to [Wikipedia](https://en.wikipedia.org/wiki/Galton_board):

The Galton board consists of a vertical board with interleaved rows of pegs. Beads are dropped from the top and, when the device is level, bounce either left or right as they hit the pegs. 
Eventually they are collected into bins at the bottom, where the height of bead columns accumulated in the bins approximate a bell curve. 
Overlaying Pascal's triangle onto the pins shows the number of different paths that can be taken to get to each bin.

![bla](docs/assets/Galton_box.jpg)

