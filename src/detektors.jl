struct Detektor
	bins::Vector{Int64}
	Detektor() = new(Vector{Int64}())
end

function detekt!(d::Detektor,xpos::Int64) 
    append!(d.bins,xpos)
    return
end

function detekt!(d::Detektor,particle::Particle)
    if particle.running
        detekt!(d,particle.xpos)
        particle.running=false
        # consider deleting particle 
    end
    return
end