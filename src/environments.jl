


abstract type AbstractEnvironment end


struct OneNodeGrid{I<:AbstractInteraction} <: AbstractEnvironment
    detektor::Detektor
    interaction::I

    function OneNodeGrid(interaction::I) where {I<:AbstractInteraction}
        return new{I}(Detektor(),interaction)
    end
end

function step!(env::G,particle::Particle) where {G<:OneNodeGrid}
	
	if goes_left(env.interaction)
		xstep = -1
	else
		xstep = 1
	end
    
    step!(particle;x=xstep,y=1)
	detekt!(env.detektor,particle)
    return 
end

struct MultiNodeGrid{I<:AbstractInteraction} <: AbstractEnvironment
    nrows::Int64
	ncols::Int64
    detektor::Detektor
    interaction::I
    
    MultiNodeGrid(nrows::Int64,inter::I) where {I<:AbstractInteraction} = new{I}(nrows,2*(nrows-1),Detektor(),inter)

end


function step!(env::MultiNodeGrid,particle::Particle)
    if (particle.ypos+1)>=env.nrows
        detekt!(env.detektor,particle)
        return
    else
        if goes_left(env.interaction)
            xstep = -1
        else
            xstep = 1
        end
    end

    

    if abs(particle.xpos + xstep) >= env.nrows-1
        detekt!(env.detektor,particle)
    else
        step!(particle;x=xstep,y=1)
    end


    return 
end


struct InfiniteGrid{I<:AbstractInteraction} <: AbstractEnvironment
    nrows::Int64
    detektor::Detektor
    interaction::I
    
    InfiniteGrid(nrows::Int64,inter::I) where {I<:AbstractInteraction} = new{I}(nrows,Detektor(),inter)
end

InfiniteGrid(nrows::Int64) = InfiniteGrid(nrows,BinomialInteraction())

function step!(env::InfiniteGrid,particle::Particle)
    if particle.ypos+1>=env.nrows
        detekt!(env.detektor,particle)
        return
    else
        if goes_left(env.interaction)
            xstep = -1
        else
            xstep = 1
        end
    end

    step!(particle;x=xstep,y=1)
    return 
end


function step!(env::AbstractEnvironment,particles::Particle...)
    for particle in particles
        step!(env,particle)
    end
end