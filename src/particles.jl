


mutable struct Particle
    xpos::Int64
    ypos::Int64
    running::Bool

    Particle() = new(0,0,true)
end

function step!(particle::Particle; x::Int64=0,y::Int64=0)
    if particle.running
        particle.xpos += x
        particle.ypos += y
    end
    return 
end