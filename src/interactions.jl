const GLOBAL_SEED = 12345678
const GLOBAL_RNG = MersenneTwister(GLOBAL_SEED)

abstract type AbstractInteraction end

struct BinomialInteraction <: AbstractInteraction
	left_propability::Float64
	
	function BinomialInteraction(p::Float64) 
		if 0.0<=p<=1.0 || error("Propability needs to be non-negative and less or equal to 1.0. <$p> given.")
			return new(p)
		end

	
	end
end

BinomialInteraction() = BinomialInteraction(0.5)

goes_left(rng::AbstractRNG,I::BinomialInteraction) = rand(rng)<=I.left_propability
goes_left(I::BinomialInteraction) = goes_left(GLOBAL_RNG,I::BinomialInteraction)
