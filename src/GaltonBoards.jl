module GaltonBoards

using Random


# Write your package code here.

export Particle
export Detektor, detekt!

export BinomialInteraction, goes_left
export InfiniteGrid, OneNodeGrid, MultiNodeGrid

export step!



include("particles.jl")
include("interactions.jl")
include("detektors.jl")
include("environments.jl")
include("simulation.jl")


end
