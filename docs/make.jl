using GaltonBoards
using Documenter

DocMeta.setdocmeta!(GaltonBoards, :DocTestSetup, :(using GaltonBoards); recursive=true)

makedocs(;
    modules=[GaltonBoards],
    authors="Uwe Hernandez Acosta",
    repo="https://gitlab.hzdr.de/hernan68/GaltonBoards.jl/blob/{commit}{path}#{line}",
    sitename="GaltonBoards.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://hernan68.gitlab.io/GaltonBoards.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
